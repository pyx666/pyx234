package com.pyxzz.myapplication.Model;

import java.util.ArrayList;

public class BannerModel {
    public String msg;
    public int code;
    public ArrayList<Data> data;
    public class Data{
        public String id;
        public String titel;
        public String bannerimg;
    }
}
