package com.pyxzz.myapplication.Model;

import java.util.ArrayList;

public class ExerciseDetailModel {
    public  String msg;
    public   int code;
    public  Detail data;
    public class Detail{
        public  String id;
        public  String title;
        public  String des;
        public  String time;
        public  String img;
        public  String subtitle;
        public ArrayList<Date> subjectList;
    }



    public class Date{
        public  String id;
        public  String title;
        public  String content;
        public  String optiona;
        public  String optionb;
        public  String optionc;
        public  String optiond;
        public  String answer;


    }

}
