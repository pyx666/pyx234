package com.pyxzz.myapplication.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pyxzz.myapplication.LoginActivity;
import com.pyxzz.myapplication.R;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;
public class MyFragment extends Fragment {
    ImageView head;
    String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        head = view.findViewById(R.id.mylogin);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("bxg",Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token","");
        if (token.length()>0){
            RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/getuserinfo");
            requestParams.addBodyParameter("token",token);
            x.http().post(requestParams, new Callback.CacheCallback<String>() {
                @Override
                public void onSuccess(String result) {

                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {

                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {

                }

                @Override
                public boolean onCache(String result) {
                    return false;
                }
            });
        }
        Log.i("MyFragment",token);
        head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (token.length()>0){


                }else {
                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        return view;
    }
}