package com.pyxzz.myapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pyxzz.myapplication.Model.ExerciseDetailModel;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;

public class ExerciseDetailActivity extends AppCompatActivity {
        String id;
        RecyclerView recyclerView;
        MyAdapter myAdapter;
        TextView title;
        ImageView back;
        public ArrayList<ExerciseDetailModel.Date> subjectList = new ArrayList<>();
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_exercise_detail);
            if (getIntent()!= null &&getIntent().getStringExtra("id")!= null){
                id = getIntent().getStringExtra("id");
            }
            title = findViewById(R.id.title);
            back = findViewById(R.id.back);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();;
                }
            });
            recyclerView = findViewById(R.id.recycleview);
            myAdapter = new MyAdapter();
            recyclerView.setAdapter(myAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(ExerciseDetailActivity.this));
            RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/exercisedetail");
            requestParams.addBodyParameter("id",id);

            x.http().post(requestParams, new Callback.CacheCallback<String>() {
                @Override
                public void onSuccess(String result) {
                    Gson gson = new Gson();
                    ExerciseDetailModel exerciseDetailModel = gson.fromJson(result,ExerciseDetailModel.class);
                    subjectList = exerciseDetailModel.data.subjectList;
                    myAdapter.notifyDataSetChanged();
                    title.setText(exerciseDetailModel.data.title);
                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {

                }

                @Override
                public void onCancelled(CancelledException cex) {

                }

                @Override
                public void onFinished() {

                }

                @Override
                public boolean onCache(String result) {
                    return false;
                }
            });


        }


        public class  MyAdapter extends RecyclerView.Adapter<MyViewHoder>{
            @NonNull
            @Override
            public MyViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view =View.inflate(ExerciseDetailActivity.this,R.layout.exercise_detail_item_layout,null);
                MyViewHoder myViewHoder = new MyViewHoder(view);
                return  myViewHoder;
            }

            @Override
            public void onBindViewHolder(@NonNull final MyViewHoder myViewHoder, final int i) {
                myViewHoder.title.setText( "  " +i +" "+subjectList.get(i).content);
                myViewHoder.texta.setText(subjectList.get(i).optiona);
                myViewHoder.textb.setText(subjectList.get(i).optionb);
                myViewHoder.textc.setText(subjectList.get(i).optionc);
                myViewHoder.textd.setText(subjectList.get(i).optiond);
                myViewHoder.imga.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subjectList.get(i).answer.equals("1")){
                            myViewHoder.imga.setBackgroundResource(R.drawable.exercises_right_icon);
                        }else {
                            myViewHoder.imga.setBackgroundResource(R.drawable.exercises_error_icon);
                        }
                        if (subjectList.get(i).answer.equals("2")){
                            myViewHoder.imgb.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("3")){
                            myViewHoder.imgc.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("4")){
                            myViewHoder.imgd.setBackgroundResource(R.drawable.exercises_right_icon);
                        }

                    }
                });

                myViewHoder.imgb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subjectList.get(i).answer.equals("1")){
                            myViewHoder.imga.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("2")){
                            myViewHoder.imgb.setBackgroundResource(R.drawable.exercises_right_icon);
                        }else {
                            myViewHoder.imgb.setBackgroundResource(R.drawable.exercises_error_icon);
                        }
                        if (subjectList.get(i).answer.equals("3")){
                            myViewHoder.imgc.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("4")){
                            myViewHoder.imgd.setBackgroundResource(R.drawable.exercises_right_icon);
                        }

                    }
                });
                myViewHoder.imgc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subjectList.get(i).answer.equals("1")){
                            myViewHoder.imga.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("2")){
                            myViewHoder.imgb.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("3")){
                            myViewHoder.imgc.setBackgroundResource(R.drawable.exercises_right_icon);
                        }else {
                            myViewHoder.imgc.setBackgroundResource(R.drawable.exercises_error_icon);
                        }
                        if (subjectList.get(i).answer.equals("4")){
                            myViewHoder.imgd.setBackgroundResource(R.drawable.exercises_right_icon);
                        }

                    }
                });
                myViewHoder.imgd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subjectList.get(i).answer.equals("1")){
                            myViewHoder.imga.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("2")){
                            myViewHoder.imgb.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("3")){
                            myViewHoder.imgc.setBackgroundResource(R.drawable.exercises_right_icon);
                        }
                        if (subjectList.get(i).answer.equals("4")){
                            myViewHoder.imgd.setBackgroundResource(R.drawable.exercises_right_icon);
                        }else {
                            myViewHoder.imgd.setBackgroundResource(R.drawable.exercises_error_icon);
                        }

                    }
                });

            }

            @Override
            public int getItemCount() {
                return subjectList.size();
            }
        }


        public class MyViewHoder extends RecyclerView.ViewHolder{
            ImageView imga,imgb,imgc,imgd;
            TextView texta,textb,textc,textd,title;
            public MyViewHoder(@NonNull View itemView) {
                super(itemView);
                imga = itemView.findViewById(R.id.imga);
                imgb = itemView.findViewById(R.id.imgb);
                imgc = itemView.findViewById(R.id.imgc);
                imgd = itemView.findViewById(R.id.imgd);
                texta = itemView.findViewById(R.id.texta);
                textb = itemView.findViewById(R.id.textb);
                textc = itemView.findViewById(R.id.textc);
                textd = itemView.findViewById(R.id.textd);
                title = itemView.findViewById(R.id.title);
            }
        }
    }