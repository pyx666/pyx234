package com.pyxzz.myapplication.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pyxzz.myapplication.ExerciseDetailActivity;
import com.pyxzz.myapplication.Model.ExerciseModel;
import com.pyxzz.myapplication.R;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;

public class ExerciseFragment extends Fragment {
    ListView mListview;
    MyAdapter myAdapter;
    ArrayList<ExerciseModel.Data> lists = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise, container, false);
        mListview = view.findViewById(R.id.listview);
        myAdapter = new MyAdapter();
        mListview.setAdapter(myAdapter);
        RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/exerciselist");
        x.http().post(requestParams, new Callback.CacheCallback<String>() {

            @Override
            public void onSuccess(String result) {

                Gson gson = new Gson();
                ExerciseModel exerciseModel = gson.fromJson(result, ExerciseModel.class);
                Log.i("ExerciseFragment", result);
                if (exerciseModel != null && exerciseModel.data != null) {
                    lists = exerciseModel.data;
                    myAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Log.i("ExerciseFragment", ex.getMessage());
            }


            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
            });
        return view;
    }

    public class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return lists.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ExerciseModel.Data data = lists.get(position);
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.list_item, null);
                viewHolder.mPostionTv = convertView.findViewById(R.id.text1);
                viewHolder.titleTv = convertView.findViewById(R.id.text2);
                viewHolder.countTv = convertView.findViewById(R.id.text3);
                viewHolder.rootLayout = convertView.findViewById(R.id.root);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.mPostionTv.setText(position + 1 + "");
            viewHolder.rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ExerciseDetailActivity.class);
                    intent.putExtra("id",data.id);
                    startActivity(intent);

                }
            });
            viewHolder.titleTv.setText(data.title);
            viewHolder.countTv.setText("共计5题");
            return convertView;
        }

        public class ViewHolder {
            public TextView mPostionTv;
            public TextView titleTv;
            public TextView countTv;
            public ConstraintLayout rootLayout;

        }
    }
}

