package com.pyxzz.myapplication.Model;

import java.util.ArrayList;

public class CourseDetailModel {
    public  String msg;
    public   int code;
    public  Detail data;
    public class Detail{
        public  String id;
        public  String title;
        public  String des;
        public  String time;
        public  String img;
        public  String subtitle;
        public ArrayList<Date> videoList;
    }

    public class Date{
        public  String id;
        public  String videotitle;
        public  String videourl;
        public  String chaptervideoid;


    }
}
