package com.pyxzz.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pyxzz.myapplication.Model.CourseDetailModel;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;

public class CouseDetailActivity extends AppCompatActivity {

    ImageView img;
    TextView textView,desTv,videoTv;
    RecyclerView recyclerView;
    String id;
    MyAdatper myAdatper;
    public ArrayList<CourseDetailModel.Date> videoList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_couse_detail);
        img = findViewById(R.id.img);
        textView = findViewById(R.id.detaltv);
        recyclerView = findViewById(R.id.recycleview);
        recyclerView.setAdapter(myAdatper);
        myAdatper = new MyAdatper();
        recyclerView.setAdapter(myAdatper);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setVisibility(View.GONE);
        desTv = findViewById(R.id.destv);

        desTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desTv.setSelected(true);
                videoTv.setSelected(false);
                textView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
//                recyclerView.setVisibility(View.INVISIBLE);
            }
        });
        videoTv = findViewById(R.id.videotv);
        desTv.setSelected(true);
        videoTv.setSelected(false);
        videoTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desTv.setSelected(false);
                videoTv.setSelected(true);
                textView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });


        if (getIntent() != null && getIntent().getStringExtra("id") != null) {
            id = getIntent().getStringExtra("id");
        }

        RequestParams requestParams = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/chapterdetail");
        requestParams.addBodyParameter("id", id);

        x.http().post(requestParams, new Callback.CacheCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                CourseDetailModel courseDetailModel = gson.fromJson(result, CourseDetailModel.class);
                Log.i("CouseDetailActivity", result);
                x.image().bind(img, courseDetailModel.data.img);
                textView.setText(courseDetailModel.data.des);
                videoList = courseDetailModel.data.videoList;
                myAdatper.notifyDataSetChanged();
//             title.setText(exerciseDetailModel.data.title);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
        });
    }

        public class MyAdatper extends RecyclerView.Adapter<MyViewHoder>{

            @NonNull
            @Override
            public MyViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = View.inflate(CouseDetailActivity.this,R.layout.couse_detail_item_layout,null);
                MyViewHoder viewHoder = new MyViewHoder(view);
                return viewHoder;
            }

            @Override
            public void onBindViewHolder(@NonNull MyViewHoder myViewHoder, final int i) {
                myViewHoder.text.setText(videoList.get(i).videotitle);
                myViewHoder.mRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CouseDetailActivity.this,VideoActivity.class);
                        intent.putExtra("videourl",videoList.get(i).videourl);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public int getItemCount() {
                return videoList.size();
            }
        }

        public class MyViewHoder extends RecyclerView.ViewHolder{
            LinearLayout mRoot;
            TextView text;
            public MyViewHoder(@NonNull View itemView) {
                super(itemView);
                text= itemView.findViewById(R.id.titletv);
                mRoot=itemView.findViewById(R.id.videobtn);
            }

    }

}
