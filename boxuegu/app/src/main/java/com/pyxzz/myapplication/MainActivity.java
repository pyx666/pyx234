package com.pyxzz.myapplication;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.pyxzz.myapplication.fragment.CourseFragment;
import com.pyxzz.myapplication.fragment.ExerciseFragment;
import com.pyxzz.myapplication.fragment.MyFragment;

public class MainActivity extends AppCompatActivity {
    RelativeLayout mCourseBtn,mExerciseBtn,myBtn;
    Fragment coursefragment = new CourseFragment();
    Fragment exercisefragment = new ExerciseFragment();
    Fragment myfragment = new MyFragment();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout,coursefragment).commit();
        mExerciseBtn = findViewById(R.id.exerciselayout);
        mExerciseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,exercisefragment).commit();
                mCourseBtn.setSelected(false);
                mExerciseBtn.setSelected(true);
                myBtn.setSelected(false);
            }
        });
       mCourseBtn = findViewById(R.id.couselayout);
       mCourseBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               getSupportFragmentManager().beginTransaction().replace(R.id.layout,coursefragment).commit();
               mCourseBtn.setSelected(true);
               mExerciseBtn.setSelected(false);
               myBtn.setSelected(false);
           }
       });
        myBtn = findViewById(R.id.mylayout);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.layout,myfragment).commit();
                 mCourseBtn.setSelected(false);
                 mExerciseBtn.setSelected(false);
                 myBtn.setSelected(true);
            }
        });

        mCourseBtn.setSelected(true);
        mExerciseBtn.setSelected(false);
        myBtn.setSelected(false);
        }
    }

