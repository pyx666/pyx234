package com.pyxzz.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pyxzz.myapplication.CouseDetailActivity;
import com.pyxzz.myapplication.Model.BannerModel;
import com.pyxzz.myapplication.Model.ExerciseModel;
import com.pyxzz.myapplication.R;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.ArrayList;

public class CourseFragment extends Fragment {
    ViewPager mViewpager;
    MyViewPager myViewPager;
    LinearLayout mDotLayout;

    RecyclerView mRecyclerView;
    RecycleAdapter myAdapter;
    public ArrayList<BannerModel.Data> bannerlist = new ArrayList<>();
    ArrayList<ExerciseModel.Data> list = new ArrayList<>();
    ArrayList<ImageView> imageViews = new ArrayList<>();
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            handler.sendEmptyMessageDelayed(1,2000);
            mViewpager.setCurrentItem(mViewpager.getCurrentItem()+1);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course, container, false);
        mViewpager = view.findViewById(R.id.viewpager);
        mDotLayout = view.findViewById(R.id.dotlayout);
        myViewPager = new MyViewPager();
        mRecyclerView = view.findViewById(R.id.recyclerView);
        myAdapter = new RecycleAdapter();
        mRecyclerView.setAdapter(myAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mViewpager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i==0){
                    mViewpager.setCurrentItem(bannerlist.size()-2,false);
                }
                if (i==bannerlist.size()-1){

                    mViewpager.setCurrentItem(1,false);
                }
                setDots();
                Log.i("CourseFragment","CourseFragment"+1);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        RequestParams params = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/banner");
        x.http().post(params, new Callback.CacheCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                BannerModel bannerModel = gson.fromJson(result, BannerModel.class);
                if (bannerModel != null && bannerModel.data != null) {
                    bannerlist.addAll(bannerModel.data);
                    bannerlist.add(bannerModel.data.get(0));
                    bannerlist.add(0, bannerModel.data.get(bannerModel.data.size() - 1));
                    mViewpager.setAdapter(myViewPager);
                    mViewpager.setCurrentItem(1);
                    handler.sendEmptyMessageDelayed(1,2000);
                    for (int i = 0;i < bannerModel.data.size();i++){
                        ImageView imageView = new ImageView(getActivity());
                        imageView.setBackgroundResource(R.drawable.courseimg);
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(30,30);
                        params1.leftMargin=10;
                        params1.rightMargin=10;
                        imageView.setLayoutParams(params1);
                        mDotLayout.addView(imageView);
                        imageViews.add(imageView);
                    }
                }
            }
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
        });
        RequestParams params1 = new RequestParams("http://148.70.46.9:8080/boxuegu/bxg/chapterlist");
        x.http().post(params1, new Callback.CacheCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                ExerciseModel exerciseModel = gson.fromJson(result, ExerciseModel.class);
                Log.i("CourseFragment", result);
                if (exerciseModel != null && exerciseModel.data != null) {
                  list = exerciseModel.data;
//                    myAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public boolean onCache(String result) {
                return false;
            }
        });
        return view;

    }
    public void setDots(){
       for (int i = 0;i< imageViews.size();i++){
           if (i==mViewpager.getCurrentItem()-1){
               imageViews.get(i).setSelected(true);
           }else {
               imageViews.get(i).setSelected(false);
           }
       }

    }
public class MyViewPager extends PagerAdapter{

    @Override
    public int getCount() {
        return bannerlist.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.recomputeViewAttributes((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(getActivity());
        x.image().bind(imageView,bannerlist.get(position).bannerimg);
        container.addView(imageView);
//        TextView textView = new TextView(getActivity());
//        textView.setText(position +"asdgasg");
//        textView.setGravity(Gravity.CENTER);
//        textView.setTextSize(30);
//        container.addView(textView);
        return imageView;
    }
}
public class  RecycleAdapter extends RecyclerView.Adapter<MyViewHoder>{

    @NonNull
    @Override
    public MyViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(getActivity(),R.layout.course_item_layout,null);
        MyViewHoder myViewHoder = new MyViewHoder(view);
        return myViewHoder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHoder myViewHoder, final int i) {
        myViewHoder.titletv.setText(list.get(i).title);
        myViewHoder.contenttv.setText(list.get(i).content);
        myViewHoder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), CouseDetailActivity.class);
                intent.putExtra("id", list.get(i).id);
                startActivity(intent);
            }
        });
        x.image().bind(myViewHoder.img, list.get(i).img);
    }
        @Override
    public int getItemCount() { return list.size(); }
}
public class  MyViewHoder extends  RecyclerView.ViewHolder{
        public TextView titletv;
        public ImageView img;
        public TextView contenttv;
        ConstraintLayout  mRoot;


    public MyViewHoder(@NonNull View itemView) {
        super(itemView);
        titletv = itemView.findViewById(R.id.title);
        img = itemView.findViewById(R.id.img);
        contenttv = itemView.findViewById(R.id.content);
        mRoot = itemView.findViewById(R.id.root2);

    }
}
}
