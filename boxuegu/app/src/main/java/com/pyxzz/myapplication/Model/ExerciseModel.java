package com.pyxzz.myapplication.Model;

import java.util.ArrayList;

public class ExerciseModel {
    public String msg;
    public int code;
    public ArrayList<Data> data;
    public class Data{
        public String id;
        public String title;
        public String des;
        public String img;
        public String content;
    }
}
